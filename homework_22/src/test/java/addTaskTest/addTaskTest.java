package addTaskTest;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class addTaskTest {
    private static WebDriver driver;
    private String url = "https://54.76.141.74/login";
    private String urlSignOut = "https://54.76.141.74/signOut";
    private String username = "testuser";
    private String password = "asdffdsA1";
    private String nameTask = "urgent task";
    private String nameTaskText = "text";

    private List<String> list = new ArrayList<String>();

    private String btnExit = "li.dropdown.user.user-menu.open div.pull-right > a";


    @BeforeClass
    public static void before () {
        driver = new ChromeDriver();
    }
    @Test
    public void researchToolTest () throws InterruptedException {
        driver.get(url);
        driver.findElement(By.cssSelector("input[name=\"username\"]")).click();
        driver.findElement(By.cssSelector("input[name=\"username\"]")).sendKeys(username);
        driver.findElement(By.cssSelector("input[name=\"password\"]")).click();
        driver.findElement(By.cssSelector("input[name=\"password\"]")).sendKeys(password);
        driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();

        Thread.sleep(1000);
        driver.findElement(By.cssSelector("body > aside > section > ul > li:nth-child(4)")).click();
        driver.findElement(By.cssSelector("a[ng-href=\"/#!/tasks/assign\"]")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("body > div > section.content.ng-scope > section > div:nth-child(1) > div > button")).click();
        driver.findElement(By.id("Name")).click();
        driver.findElement(By.id("Name")).sendKeys(nameTask);
        driver.findElement(By.cssSelector("form > div:nth-child(3) > select")).click();
        driver.findElement(By.cssSelector("form > div:nth-child(3) > select > option:nth-child(1)")).click();
        driver.findElement(By.cssSelector("form > div:nth-child(3) > select")).click();
        driver.findElement(By.id("Data")).click();
        driver.findElement(By.id("Data")).sendKeys(nameTaskText);
        driver.findElement(By.cssSelector("form > div.form-group.schema-form-submit > input")).click();


        Thread.sleep(2000);

        List<WebElement> elements = driver.findElements(By.cssSelector("div:nth-child(2) > div > div.ui-grid-canvas > div > div > div:nth-child(2)"));

        for (WebElement el: elements) {
            if (el.getText().toLowerCase().equals(nameTask.toLowerCase())) {
                driver.findElement(By.cssSelector("div > button.btn.btn-danger.ng-scope")).click();
                break;
            }
        }

    }
    @AfterClass
    public static void after () {
        driver.close();
    }
}
