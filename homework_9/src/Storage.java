import java.util.Arrays;

public class Storage {
    private String storage[] = new String[10];

    public void create (String user) {
        for (int i = 0; i < storage.length; i++) {
            if (storage[i] == null) {
                storage[i] = user;
                System.out.println("Created user: " + user);
                System.out.println(Arrays.toString(storage));
                break;
            }
        }
    }
    public void edit (String newUser, String oldUser) {
        for (int i = 0; i < storage.length; i++) {
            if (storage[i] == oldUser) {
                storage[i] = newUser;
                System.out.println("Edit " + oldUser + " to " + newUser);
                break;
            }
        }
    }
    public void delete (String name) {
        for (int i = 0; i < storage.length; i++) {
            if (storage[i] == name) {
                storage[i] = null;
                System.out.println("Delete " + name );
                System.out.println(Arrays.toString(storage));
                break;
            }
        }
    }
    public void getUser (String name) {
        for (int i = 0; i < storage.length; i++) {
            if (storage[i] == name) {
                System.out.println(storage[i]);
            }
        }
    }
}
