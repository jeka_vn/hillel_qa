public class User extends Storage {
    private String name;
    User() {
        super();
    }
    public void userOperations (String name, String command, String oldName) {
        this.name = name;
        switch (command) {
            case "create":
                this.create(this.name);
                break;
            case "edit":
                this.edit(this.name, oldName);
                break;
            case "delete":
                this.delete(this.name);
                break;
            case "getUser":
                this.getUser(this.name);
                break;
        }
    }
}
