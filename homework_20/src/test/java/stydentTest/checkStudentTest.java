package stydentTest;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class checkStudentTest {
    @Test
    public void checkCreateTest(WebDriver driver, String name) {

        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("ul[data-bind-component=\"UserList\"] > li > h4")));


        List<WebElement> elements = driver.findElements(By.cssSelector("ul[data-bind-component=\"UserList\"] > li > h4"));
        for (WebElement el: elements) {
            System.out.println(el.getText());
            if (el.getText().toLowerCase().equals(name.toLowerCase()))
                Assert.assertTrue(el.getText().toLowerCase().contains(name.toLowerCase()));
        }
    }
}
