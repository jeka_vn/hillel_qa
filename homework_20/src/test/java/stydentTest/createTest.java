package stydentTest;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class createTest {
    private static WebDriver driver;
    private String name = "E.Lambov";
    private String  tel = "102";

    @BeforeClass
    public static void init() {
        driver = new ChromeDriver();
    }
    @Test
    public void createStudent() {
        String url = "http://37.59.228.229:3000/";
        driver.get(url);
        driver.findElement(By.cssSelector("a.add-user")).click();
        driver.findElement(By.cssSelector("input#icon_prefix")).click();
        driver.findElement(By.cssSelector("input#icon_prefix")).clear();
        driver.findElement(By.cssSelector("input#icon_prefix")).sendKeys(name);

        driver.findElement(By.cssSelector("input#icon_telephone")).click();
        driver.findElement(By.cssSelector("input#icon_telephone")).clear();
        driver.findElement(By.cssSelector("input#icon_telephone")).sendKeys(tel);

       driver.findElement(By.cssSelector("div.col.s9.main-content > a.save-btn")).click();

       driver.navigate().refresh();

        checkStudentTest test = new checkStudentTest();
        test.checkCreateTest(driver, name);
    }
    @AfterClass
    public static void endTest() {
        driver.close();
    }
}
