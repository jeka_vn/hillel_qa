package google;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;

public class GoogleSearchTest {
    private static WebDriver driver;
    private String cssElInput = "input[name=\"q\"]";
    private String url = "https://www.google.com.ua/";
    private List<String> list = new ArrayList<String>();
    private String searchText = "Школа Hillel";
    private int wordCount = searchText.split(" ").length;
    private String[] arrSearchText = searchText.split(" ", wordCount);
    private int first = 0;
    private int two = 0;
    private int numPages = 6;

    @BeforeClass
    public static void before () {
        driver = new ChromeDriver();
    }
    @Test
    public void test () {
        driver.get(url);

        for (int i = 0; i < numPages; i++) {
            if (i == 0) {
                driver.findElement(By.cssSelector(cssElInput)).click();
                driver.findElement(By.cssSelector(cssElInput)).sendKeys(searchText);
                driver.findElement(By.cssSelector(cssElInput)).sendKeys(Keys.ENTER);
                List<WebElement> elements = driver.findElements(By.cssSelector("div.rc"));

                for (WebElement el: elements) list.add(el.getText());
            } else {
                driver.findElement(By.cssSelector("#pnnext")).click();
                List<WebElement> elements1 = driver.findElements(By.cssSelector("div.rc"));

                for (WebElement el: elements1) list.add(el.getText());
            }
        }

        for (String s : list) {
            if (s.toLowerCase().contains(arrSearchText[0].toLowerCase())) {
                first++;

                try {
                    Assert.assertTrue(s.toLowerCase().contains(arrSearchText[0].toLowerCase()));
                } catch (AssertionError e) {
                    System.out.println(s.toLowerCase());
                    System.out.println(arrSearchText[0].toLowerCase());
                }
            }
            if (s.toLowerCase().contains(arrSearchText[1].toLowerCase())) {
                two++;
                try {
                    Assert.assertTrue(s.toLowerCase().contains(arrSearchText[1].toLowerCase()));
                } catch (AssertionError e) {
                    System.out.println(s.toLowerCase());
                    System.out.println(arrSearchText[1].toLowerCase());
                }
            }
        }
        System.out.println("Собрано результатов: " + list.size());
        System.out.println("Слово: " + arrSearchText[0] + " найдено " + first + " раз.");
        System.out.println("Слово: " + arrSearchText[1] + " найдено " + two + " раз.");

    }
    @AfterClass
    public static void after () {
        driver.close();
    }
}

